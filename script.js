const express = require('express');
const app = express();

app.use(express.json());

app.post('/create-topic', (req, res) => {
    const title = req.body.title;
    const text = req.body.text;

    // TODO: сохраняем тему в базе данных и возвращаем новый объект темы в формате JSON
    const topic = {
        id: 4,
        title,
        text
    };
    res.json(topic);
});

app.listen(3000, () => {
    console.log('Server started on port 3000');
});
